<?php

namespace Drupal\Configuration;

class Configuration {
  protected $id;
  
  public function __construct($id) {
    $this->id = $id;
    
    $data = array();
    if ($id == 'plugin.core.cache') {
      $data = array(
                'factory' => '\Drupal\Plugin\Core\Cache',
                'title' => 'Cache',
                'description' => 'A core cache plugin utility mechanism.',
              );
    }
    if ($id == 'plugin.core.block') {
      $data = array(
                'factory' => '\Drupal\Plugin\Core\Multiconfig',
                'title' => 'Block',
                'description' => 'Block plugin mechanism.',
                'class_param' => 'block_class',
                'children' => TRUE,
              );
    }
    if ($id == 'plugin.core.block.test') {
      $data = array(
                'title' => 'Test Block PLugin',
                'description' => 'A test block plugins.',
                'block_class' => '\Drupal\Plugin\Core\Testblock',
              );
    }
    foreach ($data as $key => $value) {
      $this->$key = $value;
    }
  }
  
  public function save() {
    // TODO: Implement
  }
}
