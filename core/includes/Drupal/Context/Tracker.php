<?php

namespace Drupal\Context;

/**
 * Transaction-like class for the context stack.
 *
 * When this class is destroyed, so its its corresponding context object.
 */
class Tracker {

  /**
   * The context object we're tracking.
   *
   * @var ContextInterface
   */
  protected $context;

  /**
   * The stack object to which the context object we're tracking is bound.
   *
   * @var Stack
   */
  protected $stack;

  /**
   * Constructs a Tracker object.
   *
   * @param ContextInterface $context
   *   The context object we should be tracking.
   */
  public function __construct(ContextInterface $context) {
    $this->context = $context;

    // If the context object has not been bound to a stack, it won't work
    // properly.  Still, we shouldn't fatal.
    $stack = $this->context->getStack();
    if (isset($stack)) {
      $stack->push($this->context);
      $this->stack = $stack;
    }
  }

  /**
   * Destructs a Tracker object.
   *
   * When destroying this object, pop the associated context off the stack and
   * everything above it.
   *
   * Note that this method does not actively destroy those context objects, it
   * just pops them off the stack. PHP will delete them for us unless someone
   * has one hanging around somewhere.
   */
  public function __destruct() {
    if ($this->stack) {
      $this->stack->pop($this->context);
    }
  }
}
