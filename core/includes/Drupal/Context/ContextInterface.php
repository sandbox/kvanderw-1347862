<?php

namespace Drupal\Context;

use Closure;

/**
 * Interface definition for all context objects.
 */
interface ContextInterface {

  /**
   * Sets owner stack.
   *
   * @param ContextStack $stack
   *   TODO needs a comment
   *
   * @throws LockedException
   *   If the current context is locked.
   */
  public function setStack(Stack $stack);

  /**
   * Gets stack, if any.
   *
   * @return Stack $stack
   *   TODO needs a comment
   */
  public function getStack();

  /**
   * Sets parent context.
   *
   * @param ContextInterface $parent
   *   TODO needs comment
   *
   * @throws LockedException
   *   If the current context is locked.
   */
  public function setParent(ContextInterface $parent);

  /**
   * Checks if this object has a parent context.
   *
   * @return bool
   */
  public function hasParent();

  /**
   * Gets parent context, if any.
   *
   * @return ContextInterface
   *   Can be NULL if current instance has no parent.
   */
  public function getParent();

  /**
   * Registers a class as the handler for a given context.
   *
   * @param string $context_key
   *   The context key to register for, such as "http:get".
   * @param Closure $closure
   *   An anonymous function that will be called to generate the handler
   *   on-demand.  Alternately, an object that implements the __invoke() magic
   *   method should work as well. The anonymous function or __invoke() method
   *   must return an object that implements \Drupal\Context\Handler\HandlerInterface.
   * @param array $params
   *   An array of configuration options for the class.
   */
  public function setHandler($context_key, $closure);

  /**
   * Finds the value directly from handlers, proceeding to the full handler
   * lookup algorithm, relatively to the given context. // TODO short summary needs to be on one line
   *
   * This method will not cache values inside the current instance but will
   * return it instead as the data can vary depending on the given context.
   *
   * This method is part of the context inheritance feature and is to be for
   * internal use only.
   *
   * @param string $context_key
   *   The context key to retrieve.
   * @param Context $context
   *   The context object against which to run various handlers.
   *
   * @return mixed
   *   The value that is associated with the context key. It may be a primitive
   *   value or an instance of \Drupal\Context\ValueInterface.
   */
  public function findValue($context_key, ContextInterface $context);

  /**
   * Sets an explict value for a context key.
   *
   * @param string $context_key
   *   The context key to set.
   * @param mixed $value
   *   The value to which to set the context key.  It may be any primitive value
   *   or an instance of \Drupal\Context\ValueInterface.
   */
  public function setValue($context_key, $value);

  /**
   * Retrieves the value for the specified context key.
   *
   * The context key is a colon-delimited string.  If no literal value or
   * handler has been set for that value, the right-most fragment of the key
   * will be stripped off and used as a parameter to a handler on the remaining
   * key.  That process continues until either a value is found or the key runs
   * out.
   *
   * @param string $context_key
   *   The context key to retrieve.
   *
   * @return mixed
   *   The value that is associated with the context key. It may be a primitive
   *   value or an instance of \Drupal\Context\ValueInterface.
   */
  public function getValue($context_key);

  /**
   * Returns a set of keys to objects used in the current context
   *
   * This converts any context values referenced in the current scope into
   * a normalised array.
   *
   * @return an array of context keys and their corresponding values
   */
  public function usedKeys();

  /**
   * Locks this context object against futher modification.
   *
   * This allows us to setup a mocked context object very easily, and then
   * make it immutable so we know that it won't change out from under us.
   */
  public function lock();

  /**
   * Spawns a new context object that is pushed to the context stack.
   *
   * @return DrualContextInterface
   */
  public function addLayer();
}
