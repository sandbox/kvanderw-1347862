<?php

namespace Drupal\Context\Handler;

use \Drupal\Context\ContextInterface;

/**
 * System path Context Handler implementation.
 *
 * This implementation is likely to change once the path system is no longer
 * function based.
 *
 * Examples:
 * - http://example.com/node/306 returns "node/306".
 * - http://example.com/drupalfolder/node/306 returns "node/306" while
 *   base_path() returns "/drupalfolder/".
 * - http://example.com/path/alias (which is a path alias for node/306) returns
 *   "node/306" as opposed to the path alias.
 *
 * This handler is not available in hook_boot() so use
 * drupal_get_context()->getValue('path:raw') instead.
 * However, be careful when doing that because in the case of Example #3
 * drupal_get_context()->getValue('path:raw') will contain "path/alias". If
 * "node/306" is needed, calling drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL)
 * makes this function available.

 * @todo Clean this up once the path system changes.
 */
class HandlerPathSystem extends HandlerAbstract {

  /**
   * Implements HandlerInterface::getValue().
   */
  public function getValue(array $args = array(), ContextInterface $context = null) {
    $system_path = '';

    $path = $context->getValue('path:raw');
    if (!empty($path)) {
      $system_path = drupal_get_normal_path($path);
    }
    else {
      $system_path = drupal_get_normal_path(variable_get('site_frontpage', 'node'));
    }

    return $system_path;
  }
}
