<?php

namespace Drupal\Context\Handler;

use \Drupal\Context\ContextInterface;
use \Symfony\Component\HttpFoundation\Request;

/**
 * HTTP Context Handler implementation.
 */
class HandlerHttp extends HandlerAbstract {

  /**
   * Symfony Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request = NULL;

  /**
   * Constructs a HandlerHttp object.
   *
   * @param Request $request
   *   // TODO
   */
  public function __construct(Request $request) {
    $this->request = $request;
  }

  /**
   * Implements HandlerInterface::getValue().
   */
  public function getValue(array $args = array(), ContextInterface $context = null) {
    $property = $args[0];

    switch ($property) {
      case 'method':
        $value = $this->request->getMethod();
        break;
      case 'uri':
        $value = $this->request->getUri();
        break;
      case 'base_url':
        $value = $this->request->getScheme() . '://' . $this->request->getHost() . $this->request->getBaseUrl();
        break;
      case 'base_path':
        $value = $this->request->getBasePath() . '/';
        break;
      case 'request_uri':
        $value = $this->request->getRequestUri();
        break;
      case 'script_name':
        $value = $this->request->getScriptName();
        break;
      case 'php_self':
        $value = $this->request->server->get('PHP_SELF');
        break;
      case 'accept_types':
        $value = $this->request->getAcceptableContentTypes();
        break;
      case 'domain':
        $value = $this->request->getHost();
        break;
      case 'request_args':
        $value = $this->request->request->all();
        break;
      case 'query':
        $value = $this->request->query->all();
        break;
      case 'languages':
        // Although HttpFoundation has a getLanguages() method already, it
        // does some case folding that is incompatible with Drupal's language
        // string formats.
        // @todo Revisit this after https://github.com/symfony/symfony/issues/2468
        // is resolved
        $value = $this->request->splitHttpAcceptHeader($this->request->headers->get('Accept-Language'));
        break;
      case 'files':
        $value = $this->request->files->all();
        break;
      case 'cookies':
        $value = $this->request->cookies->all();
        break;
      case 'headers':
        $value = $this->request->headers->all();
        // Cleanup from unnecessary nesting level.
        foreach ($value as &$item) {
          if (is_array($item)) {
            $item = current($item);
          }
        }
        break;
      case 'server':
        $value = $this->request->server->all();
        break;
      case 'request_body':
        $value = $this->request->getContent();
        break;
      default:
        return;
    }

    // Many of the properties of the HTTP handler are actually arrays that
    // we never want directly, but want a specific element out of. For instance,
    // http:query is an array of GET parameters.  The following routine lets us
    // retrieve third-level properties consistently, so $_GET['foo'] would map
    // to http:query:foo.  The same code also supports cookies, headers, and so
    // forth.
    // Only one level of nesting is supported.  If a GET parameter is itself an
    // array, it will be returned as an array.
    if (!is_array($value) || !isset($args[1])) {
      return $value;
    }
    else {
      // Return second nesting level value if it exists.
      if (!empty($args[1]) && isset($value[$args[1]])) {
        return $value[$args[1]];
      }
      else {
        // We return empty string if there is no
        // second argument key in $this->params[$property].
        return '';
      }
    }
  }
}
