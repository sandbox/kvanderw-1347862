<?php

namespace Drupal\Context;

/**
 * Needs a comment
 */
class Stack {

  /**
   * Internal context stack.
   *
   * @var array
   */
  protected $stack = array();

  /**
   * Pushes a new context on top of the stack, make it being the active context.
   *
   * @param ContextInterface $context
   *   // TODO comment
   */
  public function push(ContextInterface $context) {
    $this->stack[spl_object_hash($context)] = $context;
  }

  /**
   * Removes the given context from the stack, if exists, and all its children.
   *
   * @param ContextInterface $context
   *   // TODO comment
   *
   * @throws ContextException
   */
  public function pop(ContextInterface $context) {
    $pos = array_search(spl_object_hash($context), array_keys($this->stack));
    if (FALSE !== $pos && count($this->stack) > 1) {
      $this->stack = array_slice($this->stack, 0, $pos, TRUE);
    }
    else {
      // throw new ContextException("Attempting to pop a non existing context from the stack.");
    }
  }

  /**
   * Gets the top-most context object.
   *
   * The top most context object is the only one that any outside systems
   * should ever access.
   *
   * @return Drupal\Context\ContextInterface
   */
  public function getActiveContext() {
    return end($this->stack);
  }
}
