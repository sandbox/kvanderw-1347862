<?php

namespace Drupal\Context;

use Closure;

/**
 * Default Drupal context object.
 *
 * It handles routing of context requests to handlers.
 */
class Context implements ContextInterface {

  /**
   * The query string for this page. This generally means the value of $_GET['q'].
   *
   * @var string
   */
  protected $queryString;

  /**
   * Index of registered handler creation closures.
   *
   * @var array
   */
  protected $handlerClosures = array();

  /**
   * Index of already-instantiated handler objects.
   *
   * @var array
   */
  protected $handlers = array();

  /**
   * Key/value store of already-derived context information.
   *
   * @var array
   */
  protected $contextValues = array();

  /**
   * An array of keys for all the values and objects in $context accessed in
   * the current scope.
   *
   * @var array
   */
  protected $usedKeys = array();

  /**
   * Whether or not this object has been locked against further changes.
   *
   * @var boolean
   */
  protected $locked = FALSE;

  /**
   * The context stack to which this contect objec is bound.
   *
   * @var Stack
   */
  protected $stack = NULL;

  /**
   * The parent context object from which this object will inherit data.
   *
   * @var ContextInterface
   */
  protected $parent = NULL;

  /**
   * Implements ContextInterface::setStack().
   */
  public function setStack(Stack $stack) {
    if ($this->locked) {
      throw new LockedException("Cannot set the stack of a locked context.");
    }
    $this->stack = $stack;
  }

  /**
   * Implements ContextInterface::getStack().
   */
  public function getStack() {
    return $this->stack;
  }

  /**
   * Implements ContextInterface::setParent().
   */
  public function setParent(ContextInterface $parent) {
    if ($this->locked) {
      throw new LockedException("Cannot set the parent of a locked context.");
    }
    $this->parent = $parent;
    // For consistency reasons, this instance must belong to the same stack
    // than its parent.
    $this->stack = $this->parent->getStack();
  }

  /**
   * Checks if this context has a parent.
   *
   * @return bool
   *   TRUE if the context has a parent, FALSE otherwise.
   */
  public function hasParent() {
    return isset($this->parent);
  }

  /**
   * Implements ContextInterface::getParent().
   */
  public function getParent() {
    return $this->parent;
  }

  /**
   * Returns the handler at the given context key.
   *
   * @param string $context_key
   *   The context key for which we want a handler object
   *
   * @return Drupal\Context\Handler\HandlerInterface
   *   A valid handler object, or NULL if none found.
   */
  protected function getHandlerAt($context_key) {
    if (!array_key_exists($context_key, $this->handlers)) {
      if (isset($this->handlerClosures[$context_key])) {
        $this->handlers[$context_key] = $this->handlerClosures[$context_key]();
      }
      else {
        $this->handlers[$context_key] = NULL;
      }
    }

    return $this->handlers[$context_key];
  }

  /**
   * Implements ContextInterface::getValue().
   */
  public function getValue($context_key) {
    if (!$this->locked) {
      throw new NotLockedException(t('This context object has not been locked. It must be locked before it can be used.'));
    }

    // We do not have data for this offset yet: use array_key_exists() because
    // the value can be NULL. We do not want to re-run all handlerClasses for a
    // variable with data.
    if (!array_key_exists($context_key, $this->contextValues)) {
      $this->contextValues[$context_key] = $this->findValue($context_key, $this);
    }

    if (!isset($this->usedKeys[$context_key])) {
      $this->usedKeys[$context_key] = $context_key;
    }

    return $this->contextValues[$context_key];
  }

  /**
   * Implements ContextInterface::findValue().
   */
  public function findValue($context_key, ContextInterface $context) {
    // Loop over the possible context keys.
    $local_key = $context_key;
    $key_elements = explode(':', $context_key);
    $args = array();

    while ($key_elements) {
      $handler = $this->getHandlerAt($local_key);

      if (isset($handler)) {
        $handler_value = $handler->getValue($args, $context);

        // NULL value here means the context pass, and let potential parent
        // overrides happen.
        if (NULL !== $handler_value) {
          // The null object here means it's definitely a NULL and parent
          // cannot override it.
          if ($handler_value instanceof OffsetIsNull) {
            return NULL;
          }
          else {
            return $handler_value;
          }
        }
      }

      array_unshift($args, array_pop($key_elements));
      $local_key = implode(':', $key_elements);
    }

    // If we did not found a value using local handlers, check for parents.
    if ($this->hasParent()) {
      return $this->getParent()->findValue($context_key, $context);
    }

    return NULL;
  }

  /**
   * Implements ContextInterface::setValue().
   */
  public function setValue($context_key, $value) {
    if ($this->locked) {
      throw new LockedException(t('This context object has been locked. It no longer accepts new explicit context sets.'));
    }
    // Set an explicit override for a given context value.
    $this->contextValues[$context_key] = $value;
  }

  /**
   * Implements ContextInterface::setHandler().
   */
  public function setHandler($context_key, $closure) {
    if ($this->locked) {
      throw new LockedException(t('This context object has been locked. It no longer accepts new handler registrations.'));
    }
    $this->handlerClosures[$context_key] = $closure;
  }

  /**
   * Implements ContextInterface::usedKeys().
   */
  function usedKeys() {
    $key_list = array();

    foreach ($this->usedKeys as $key) {
      $value = $this->contextValues[$key];
      if ($value instanceof ValueInterface) {
        $key_list[$key] = $value->contextKey();
      }
      else {
        $key_list[$key] = $value;
      }
    }

    return $key_list;
  }

  /**
   * Implements ContextInterface::lock().
   */
  public function lock() {
    $this->locked = TRUE;
    return new Tracker($this);
  }

  /**
   * Implements ContextInterface::addLayer().
   */
  public function addLayer() {
    $layer = new self();
    $layer->setParent($this);
    return $layer;
  }
}
