<?php

namespace Drupal\Plugin;
use \Drupal\Context\Context;


/**
 * Interface definition for the mapper object.
 */
interface MapperInterface {

  /**
   * Gets a string representation of the factory class for use in the requested
   * plugin type.
   *
   * @param string $scope
   *   Provides a scoped namespace for the plugin type being requested. This
   *   allows for providers to create identically named plugin types without
   *   needing to worry about panels and views caching plugins being delivered
   *   for core cache plugin requests.
   *
   * @param string $type
   *   The plugin type for the requested scope.
   *
   * @return string
   *   The factory class name.
   */
  public function getFactoryClass($scope, $type);

  /**
   * Gets a string representation of the factory class for use in the requested
   * plugin type.
   *
   * @param string $scope
   *   Provides a scoped namespace for the plugin type being requested. This
   *   allows for providers to create identically named plugin types without
   *   needing to worry about panels and views caching plugins being delivered
   *   for core cache plugin requests.
   *
   * @param string $type
   *   The plugin type for the requested scope.
   *
   * @return string
   *   The instantiated factory class.
   */
  public function getFactory($scope, $type);

  /**
   * Instantiates a fully configured plugin instance.
   *
   * @param string $scope
   *   Provides a scoped namespace for the plugin type being requested. This
   *   allows for providers to create identically named plugin types without
   *   needing to worry about panels and views caching plugins being delivered
   *   for core cache plugin requests.
   *
   * @param string $type
   *   The plugin type for the requested scope.
   *
   * @param array $options
   *   An optional array that will be passed to the factory class's
   *   getInstance() method.
   *
   * @param Context $context
   *   An optional context object.
   *
   * @return mixed
   *   A plugin instance whose type is dependent upon the $type parameter
   *   passed to this mapper object.
   */
  public function getPlugin($scope, $type, $options = array(), Context $context = NULL);

}
