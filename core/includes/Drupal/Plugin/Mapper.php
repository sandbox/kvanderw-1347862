<?php

namespace Drupal\Plugin;
use \Drupal\Context\Context;

/**
 * Default Drupal mapper object.
 *
 * It handles mapping of plugin requests to their factory and returns plugin
 * instances.
 */
class Mapper implements MapperInterface {

  /**
   * Implements MapperInterface::getFactoryClass().
   */
  public function getFactoryClass($scope, $type) {
    $conf = config('plugin.' . $scope . '.' . $type);
    if (isset($conf->factory)) {
      return $conf->factory;
    }
    throw new MapperException("Factory class not found in configuration.");
  }

  /**
   * Implements MapperInterface::getFactory().
   */
  public function getFactory($scope, $type) {
    $factory_class = $this->getFactoryClass($scope, $type);
    if (class_exists($factory_class)) {
      $factory = new $factory_class($scope, $type);
      return $factory;
    }
    throw new MapperException("New factory class not successfully create.");
  }

  /**
   * Implements MapperInterface::getPlugin().
   */
  public function getPlugin($scope, $type, $options = array(), Context $context = NULL) {
    if (empty($context)) {
      $context = drupal_get_context();
    }
    $factory = $this->getFactory($scope, $type);
    if (method_exists($factory, 'getInstance')) {
      return $factory->getInstance($options, $context);
    }
    throw new MapperException("The getInstance method does not exist in the specified factory.");
  }

}
