<?php
/**
 * @file
 * Base exception object for grouping mapper exceptions.
 */

namespace Drupal\Plugin;

/**
 * Exception class used for mapper related errors.
 */
class MapperException extends \Exception { }
