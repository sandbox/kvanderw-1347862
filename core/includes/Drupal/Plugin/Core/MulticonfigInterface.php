<?php

namespace Drupal\Plugin\Core;
use \Drupal\Context\Context;

/**
 * Interface definition for the Multiconfig plugin factory.
 */
interface MulticonfigInterface {

  public function __construct($scope, $type);

  public function getConfiguration($options);

  /**
   * 
   */
  public function getInstance($options, Context $context);

}
