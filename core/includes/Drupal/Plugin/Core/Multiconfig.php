<?php

namespace Drupal\Plugin\Core;
use \Drupal\Context\Context;

/**
 * Default Drupal Multiconfig factory.
 *
 * Provides logic for any basic plugin type that needs to provide individual
 * plugins based upon the current available contexts.
 */
class Multiconfig implements MulticonfigInterface {
  protected $scope;
  protected $type;

  public function __construct($scope, $type) {
    $this->scope = $scope;
    $this->type = $type;
  }

  /**
   * Implements MulticonfigInterface::getConfiguration().
   */
  public function getConfiguration($options) {
    $config = config('plugin.' . $this->scope . '.' . $this->type . '.' . $options['config']);
    $type = config('plugin.' . $this->scope . '.' . $this->type);
    if (!isset($type->class_param)) {
      throw new PluginException("The plugin type class parameter is not specified.");
    }
    if (!isset($config->{$type->class_param})) {
      throw new PluginException("Plugin class was not specified.");
    }
    if (!class_exists($config->{$type->class_param})) {
      throw new PluginException("Plugin class does not exist.");
    }
    $config->plugin_class = $config->{$type->class_param};
    return $config;
  }

  /**
   * Implements MulticonfigInterface::getInstance().
   */
  public function getInstance($options, Context $context) {
    $config = $this->getConfiguration($options);
    $contexts = array();
    if (isset($options['context'])) {
      foreach ($options['context'] as $key => $value) {
        $contexts[$key] = $context->getValue($options['context']);
      }
    }
    return new $config->plugin_class($config, $contexts);
  }

}
