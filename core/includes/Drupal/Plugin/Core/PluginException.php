<?php
/**
 * @file
 * Base exception object for grouping other plugin exceptions.
 */

namespace Drupal\Plugin\Core;

/**
 * Exception class used as a base for plugin related errors.
 */
class PluginException extends \Exception { }
