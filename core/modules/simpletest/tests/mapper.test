<?php

/**
 * @file
 * Tests for Mapper class.
 */

class MapperUnitTestCase extends DrupalUnitTestCase {
  public static function getInfo() {
    return array(
      'name' => 'Mapper',
      'description' => 'Test the mapper class.',
      'group' => 'Mapper',
    );
  }
  
  /*
   * Make sure that getFactoryClass returns the proper class from Mapper.
   */
  public function testMapperGetFactoryClass() {
    $mapper = new \Drupal\Plugin\Mapper();
    $mapper_class_name = $mapper->getFactoryClass('core', 'block');
    $this->assertEqual($mapper_class_name, '\Drupal\Plugin\Core\Multiconfig', t('The correct class name was returned'), 'Plugin');
  }
  /*
   * Make sure that getFactoryClass returns the proper class from Mapper.
   */
  public function testMapperGetFactoryClassFail() {
    $mapper = new \Drupal\Plugin\Mapper();
    $mapper_class_name = $mapper->getFactoryClass('core', 'block');
    $this->assertNotEqual($mapper_class_name, '\Drupal\Plugin\Core\KlvWantsThisToFail', t('Proper failure for a non-existent Factory class'), 'Plugin');
  }
  /*
   * Make sure that getFactoryClass fails when presented with a bogus plugin 
   * type.
   */
  public function testMapperGetFactoryClassFail2() {
    $mapper = new \Drupal\Plugin\Mapper();
    try {
      $mapper_class_name = $mapper->getFactoryClass('core', 'fail');
      $this->assert('fail', t('Error - Exception was not thrown'), 'Plugin');
    } 
    catch (Exception $e) {
      $this->assert('pass', t('Proper failure for invalid plugin type.'), 'Plugin');
    }
  }
  /*
   * Make sure that getFactoryClass fails when presented with a bogus provider.
   */
  public function testMapperGetFactoryClassFail3() {
    $mapper = new \Drupal\Plugin\Mapper();
    try {
      $mapper_class_name = $mapper->getFactoryClass('fail', 'block');
      $this->assert('fail', t('Error - Exception was not thrown'), 'Plugin');
    } 
    catch (Exception $e) {
      $this->assert('pass', t('Proper failure for invalid provider (scope/owner).'), 'Plugin');
    }
  }
  /*
   * Make sure that getFactoryClass fails when presented with a bogus provider
   * and plugin type.
   */
  public function testMapperGetFactoryClassFail4() {
    $mapper = new \Drupal\Plugin\Mapper();
    try {
      $mapper_class_name = $mapper->getFactoryClass('fail', 'fail');
      $this->assert('fail', t('Error - Exception was not thrown'), 'Plugin');
    } 
    catch (Exception $e) {
      $this->assert('pass', t('Proper failure for invalid provider (scope/owner) and plugin type.'), 'Plugin');
    }
  }
  /* 
   * Check that getFactory returns a specific plugin type.
   */
  public function testMapperGetFactoryMulticonfig() {
    $mapper = new \Drupal\Plugin\Mapper();
    try {
      $mapper_class = $mapper->getFactory('core', 'block');
      $this->assertTrue($mapper_class instanceof \Drupal\Plugin\Core\Multiconfig, t('The Multiconfig factory was returned.'), 'Plugin');
    } 
    catch (Exception $e) {
      $this->assert('exception', t('The Multiconfig factory did not return. Exception caught - probably multiconfig factory is not defined.<br> @except', array('@except' => $e->GetMessage())), 'Plugin');
    }
  }
  /* 
   * Check that getFactory returns a specific plugin type.
   */
  public function testMapperGetFactoryCache() {
    $mapper = new \Drupal\Plugin\Mapper();
    try {
      $mapper_class = $mapper->getFactory('core', 'cache');
      $this->assertTrue($mapper_class instanceof Drupal\Plugin\Core\Cache, t('The Cache factory was returned.'), 'Plugin');
    } 
    catch (Exception $e) {
      $this->assert('exception', t('The Cache factory did not return. Exception caught - probably cache factory is not defined.<br> @except', array('@except' => $e->GetMessage())), 'Plugin');
    }
  }
  /*
   * Check that getFactory fails properly on a bogus plugin type.
   */
  public function testMapperGetFactoryFail() {
    $mapper = new \Drupal\Plugin\Mapper();
    try {
      $mapper_class = $mapper->getFactory('core','fail');
      $this->assert('fail', t('Found a non-existent class'), 'Plugin');
    } 
    catch (Exception $e) {
      $this->assert('pass', t('Proper return of failed load of bogus plugin type.<br>@except', array('@except' => $e->GetMessage())), 'Plugin');
    }
  }
  /*
   * Check that getFactory fails properly on a bogus provider.
   */
  public function testMapperGetFactoryFail2() {
    $mapper = new \Drupal\Plugin\Mapper();
    try {
      $mapper_class = $mapper->getFactory('fail','block');
      $this->assert('fail', t('Found a non-existent class'), 'Plugin');
    } 
    catch (Exception $e) {
      $this->assert('pass', t('Proper return of failed load of bogus provider.<br>@except', array('@except' => $e->GetMessage())), 'Plugin');
    }
  }
  /*
   * Check that getFactory fails properly on a bogus provider and plugin type.
   */
  public function testMapperGetFactoryFail3() {
    $mapper = new \Drupal\Plugin\Mapper();
    try {
      $mapper_class = $mapper->getFactory('fail','fail');
      $this->assert('fail', t('Found a non-existent class'), 'Plugin');
    } 
    catch (Exception $e) {
      $this->assert('pass', t('Proper return of failed load of bogus provider and plugin type.<br>@except', array('@except' => $e->GetMessage())), 'Plugin');
    }
  }
}